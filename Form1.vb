﻿Imports System.Data.SqlClient

Public Class Form1

    Const Connection = "Data Source=.\SQLEXPRESS;Initial Catalog=NewDatabase;Persist Security Info=True;User ID=sa;Password=1"

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Getparts()
        GetGrid()
        GetBalance()
        ComboBox3.SelectedIndex = 0
        ComboBox1.SelectedIndex = 0
    End Sub

    Sub Getparts()
        Dim da As New SqlDataAdapter("select DISTINCT PartName from dbo.Activity ", Connection)
        Dim dt As New DataTable()
        da.Fill(dt)
        ComboBox2.DataSource = dt
        ComboBox2.DisplayMember = "PartName"


    End Sub
    Sub GetGrid()
        Dim da As New SqlDataAdapter("select *  from dbo.Activity ", Connection)
        Dim dt As New DataTable()
        da.Fill(dt)
        DataGridView1.DataSource = dt
    End Sub
    Sub GetBalance()
        Dim da As New SqlDataAdapter("select sum(MoneyIn) - SUM(MonyOut) from dbo.Activity where Drawer = '" & ComboBox1.SelectedIndex & "'  ", Connection)
        Dim dt As New DataTable()
        da.Fill(dt)
        If IsDBNull(dt.Rows(0)(0)) = False Then
            TextBox1.Text = dt.Rows(0)(0)

        End If


    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox2.Text = "" Then
            Exit Sub
        End If
        Dim Amount As Double
        If Double.TryParse(TextBox2.Text, Amount) Then
            Dim Con As New SqlConnection(Connection)
            Dim AmountIn As Double = 0
            Dim AmountOut As Double = 0
            If ComboBox3.SelectedIndex = 0 Then
                AmountIn = Amount
            End If
            If ComboBox3.SelectedIndex = 1 Then
                AmountOut = Amount
            End If



            Dim CMD As New SqlCommand("Insert into Activity (Drawer,PartName,MoneyIn,MonyOut)
                               values (  '" & ComboBox1.SelectedIndex & "',
                                         '" & ComboBox2.Text & "',
                                         '" & AmountIn & "',
                                         '" & AmountOut & "'       )  ", Con)
            Con.Open()
            CMD.ExecuteNonQuery()
            Con.Close()
            GetBalance()
            GetGrid()

        End If



    End Sub
End Class
